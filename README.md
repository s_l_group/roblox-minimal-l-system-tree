# Minimal LSystem Tree

![Alt text](lsystem_tree.png)

## Description
This Roblox project generates randomized L-system tree-like structures. It accomplishes this by rewriting and modeling a string of characters according to a set of rules. It also dynamically extends existing branches when possible instead of needlessly creating a new branch.

Play it on <a href="https://www.roblox.com/games/17032001578/Minimal-L-System-Tree">Roblox</a>!
