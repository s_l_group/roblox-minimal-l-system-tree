local SS = game:GetService("ServerStorage")

local serverBranch = SS:WaitForChild("Branch")
local serverLeaf = SS:WaitForChild("Leaf")
local treeFolder = workspace:WaitForChild("Tree")


local BRANCH_LENGTH_TRIM_RANGE_MAX = math.random(6, 13) -- lower == longer branches == taller tree

local L_BRANCH_ANGLE = math.rad(22.5)
local L_BRANCH_ANGLES = {
  ["+"] = CFrame.Angles(0, L_BRANCH_ANGLE, 0),
  ["-"] = CFrame.Angles(0, -L_BRANCH_ANGLE, 0),
  ["&"] = CFrame.Angles(0, 0, L_BRANCH_ANGLE),
  ["^"] = CFrame.Angles(0, 0, -L_BRANCH_ANGLE),
  ["\\"] = CFrame.Angles(L_BRANCH_ANGLE, 0, 0),
  ["/"] = CFrame.Angles(-L_BRANCH_ANGLE, 0, 0),
  ["|"] = CFrame.Angles(0, math.rad(180), 0)
}
local L_INVERSE_BRANCH_ANGLES = {
  ["+"] = CFrame.Angles(0, -L_BRANCH_ANGLE, 0),
  ["-"] = CFrame.Angles(0, L_BRANCH_ANGLE, 0),
  ["&"] = CFrame.Angles(0, 0, -L_BRANCH_ANGLE),
  ["^"] = CFrame.Angles(0, 0, L_BRANCH_ANGLE),
  ["\\"] = CFrame.Angles(-L_BRANCH_ANGLE, 0, 0),
  ["/"] = CFrame.Angles(L_BRANCH_ANGLE, 0, 0),
  ["|"] = CFrame.Angles(0, math.rad(-180), 0),
}

local L_BRANCH_SCALE_RANGE = {
  min = Vector3.new(1.4, 1.2, 1.4),
  max = Vector3.new(1.8, 1.5, 1.8)
}
local L_BRANCH_SCALE = Vector3.new(
  math.random() * (L_BRANCH_SCALE_RANGE.max.X - L_BRANCH_SCALE_RANGE.min.X),
  math.random() * (L_BRANCH_SCALE_RANGE.max.Y - L_BRANCH_SCALE_RANGE.min.Y),
  math.random() * (L_BRANCH_SCALE_RANGE.max.Z - L_BRANCH_SCALE_RANGE.min.Z)
) + L_BRANCH_SCALE_RANGE.min

local TREE_OVERLAP = OverlapParams.new()
TREE_OVERLAP.FilterDescendantsInstances = {treeFolder}
TREE_OVERLAP.FilterType = Enum.RaycastFilterType.Include


local function rule(char, rules)
  if rules[char] then
    if char ~= "F" then
      return rules[char]
    elseif math.random(1, BRANCH_LENGTH_TRIM_RANGE_MAX) == 1 then
      return rules[char]
    end
  end
  
  return char
end

local function newLeaf(currentCFrame)
  local newLeaf = serverLeaf:Clone()
  newLeaf.CFrame = currentCFrame:ToWorldSpace(CFrame.new(0, serverLeaf.Size.Y / 2, 0))
  newLeaf.Parent = treeFolder
end

local function newBranch(currentCFrame)
  local newBranch = serverBranch:Clone()
  newBranch.CFrame = currentCFrame:ToWorldSpace(CFrame.new(0, serverBranch.Size.Y / 2, 0))
  newBranch.Parent = treeFolder
  
  return currentCFrame:ToWorldSpace(CFrame.new(0, serverBranch.Size.Y, 0))
end

-- combine/reuse branches with same rotation by extending size and moving to new center to cut costs
local function growBranch(connBranch, currentCFrame)
  connBranch.Size = Vector3.new(connBranch.Size.X, connBranch.Size.Y + serverBranch.Size.Y, connBranch.Size.Z)
  currentCFrame = currentCFrame:ToWorldSpace(CFrame.new(0, serverBranch.Size.Y, 0))
  connBranch.CFrame = currentCFrame:ToWorldSpace(CFrame.new(0, -connBranch.Size.Y / 2, 0))
  
  return currentCFrame
end

local function getBranches(currentCFrame)
  local partsOverlapping = workspace:GetPartBoundsInBox(currentCFrame, Vector3.new(5, 15, 5), TREE_OVERLAP)

  for _, b in ipairs(partsOverlapping) do
    if b.Name == "Branch" then
      local dist = (currentCFrame.Position - b.CFrame:ToWorldSpace(CFrame.new(0, b.Size.Y / 2, 0)).Position).Magnitude

      if b.Size.X == serverBranch.Size.X and b.Size.Z == serverBranch.Size.Z and dist <= 1 then
        return b
      end
    end
  end

  return nil
end



local LModule = {}

LModule.MAXPARTS = 150000
LModule.L_TRUNK_THRESHOLD = 2

function LModule.rewrite(totalString, rules)
  local newTotalString = ""
  
  for i = 1, totalString:len() do
    local char = totalString:sub(i, i)
    newTotalString = newTotalString .. rule(char, rules)

    if i % 1000 == 0 then
      wait()
    end
  end
  
  return newTotalString
end

function LModule.draw(totalString, state, currentCFrame, lExclusion)
  for i = 1, totalString:len() do
    local char = totalString:sub(i, i)
    state[#state + 1] = char
    
    if table.find(lExclusion, char) then
      if char == "L" then
        if totalString:len() >= i + 1 and totalString:sub(i + 1, i + 1) == "A" then
          newLeaf(currentCFrame)
        end
      else
        local connBranch = getBranches(currentCFrame)
        if connBranch then
          currentCFrame = growBranch(connBranch, currentCFrame)
        else
          currentCFrame = newBranch(currentCFrame)
        end
      end
    elseif L_BRANCH_ANGLES[char] then
      currentCFrame = currentCFrame:ToWorldSpace(L_BRANCH_ANGLES[char])
    elseif char == "[" then
      serverBranch.Size /= L_BRANCH_SCALE
    elseif char == "]" then
      local donePopping = false
      
      while not donePopping do
        local s = state[#state]
        
        if s == "[" then
          donePopping = true
          serverBranch.Size *= L_BRANCH_SCALE
        elseif table.find(lExclusion, s) and s ~= "L" then
          currentCFrame = currentCFrame:ToWorldSpace(CFrame.new(0, -serverBranch.Size.Y, 0))
        elseif L_INVERSE_BRANCH_ANGLES[s] then
          currentCFrame = currentCFrame:ToWorldSpace(L_INVERSE_BRANCH_ANGLES[s])
        end
        
        state[#state] = nil
      end
    end

    if i % 1000 == 0 then
      wait()
    end
  end
  
  return {["state"] = state, ["currentCFrame"] = currentCFrame}
end

return LModule
